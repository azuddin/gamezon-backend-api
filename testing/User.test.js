const { expect } = require("chai");
const faker = require("faker");

const url = `http://localhost:3001/graphql`;
const request = require("supertest")(url);

let token;

const email = "test_" + faker.internet.email();

describe("Test User Module", () => {
  it("Create a user", done => {
    request
      .post("/graphql")
      .send({
        query: `mutation CreateUser($email:String!, $password:String!) {
          createUser(userInput:{email:$email, password:$password}){
            _id
            email
            password
          }
        }`,
        variables: {
          email,
          password: "zaq1xsw2"
        }
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        if (res.body.errors) {
          expect(res.body.errors[0].message).eql("User already exists.");
        } else {
          expect(res.body.data.createUser).to.have.property("_id");
          expect(res.body.data.createUser).to.have.property("email");
          expect(res.body.data.createUser).to.have.property("password");
        }
        done();
      });
  });
  it("Login", done => {
    request
      .post("/graphql")
      .send({
        query: `query Login($email: String!, $password: String!) {
          login(email:$email,password:$password)
          {
            userId
            token
            tokenExpiration
          }
        }`,
        variables: {
          email,
          password: "zaq1xsw2"
        }
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.data.login).to.have.property("userId");
        expect(res.body.data.login).to.have.property("token");
        expect(res.body.data.login).to.have.property("tokenExpiration");
        token = res.body.data.login.token;
        done();
      });
  });
  it("List all user", done => {
    request
      .post("/graphql")
      .set("Authorization", `Bearer ${token}`)
      .send({
        query: `{
          users{
            _id
            email
            password
            createdEvents {
              _id
              title
            }
          }
        }`
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        if (res.body.errors) {
          expect(res.body.errors[0]).to.have.property("message");
        }
        expect(res.body.data.users).to.be.an("array");
        done();
      });
  });
});
