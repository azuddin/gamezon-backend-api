const { expect } = require("chai");
const faker = require("faker");

const { dateToString } = require("../helpers/date");

const url = `http://localhost:3001/graphql`;
const request = require("supertest")(url);

let token, eventId, bookingId;

const email = "test@domain.com";
const title = faker.lorem.sentence();
const description = faker.lorem.sentences();
const price = faker.finance.amount(1, 10, 2);
const date = dateToString(faker.date.recent());

before(async () => {
  let login = await request.post("/graphql").send({
    query: `query Login($email: String!, $password: String!) {
      login(email:$email,password:$password)
      {
        userId
        token
        tokenExpiration
      }
    }`,
    variables: {
      email,
      password: "zaq1xsw2"
    }
  });
  if (login.status === 500) {
    const signup = await request.post("/graphql").send({
      query: `mutation{
        createUser(userInput:{email:"${email}", password:"zaq1xsw2"}){
          _id
          email
          password
        }
      }`
    });
    login = await request.post("/graphql").send({
      query: `{ login(email:"${signup.body.data.createUser.email}", password:"zaq1xsw2")
      { userId token tokenExpiration } }`
    });
  }
  token = login.body.data.login.token;
  const createdEvent = await request
    .post("/graphql")
    .set("Authorization", `Bearer ${token}`)
    .send({
      query: `mutation{ createEvent(eventInput:{title:"Test ${title}", description:"${description}", price:${price}, date:"${date}"}) 
      { _id } }`
    });
  eventId = createdEvent.body.data.createEvent._id;
});

describe("Test Booking Module", () => {
  it("List all bookings (without auth)", done => {
    request
      .post("/graphql")
      .send({
        query: `{ 
          bookings {
            _id
            event {
              _id
            }
            user {
              _id
            }
            createdAt
            updatedAt
          }
        }`
      })
      .expect(500)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.errors[0].message).eql("Unauthenticated");
        done();
      });
  });
  it("List all bookings (with auth)", done => {
    request
      .post("/graphql")
      .set("Authorization", `Bearer ${token}`)
      .send({
        query: `{
          bookings {
            _id
            event {
              _id
            }
            user {
              _id
            }
            createdAt
            updatedAt
          }
        }`
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });
  it("Book an event (missing event)", done => {
    request
      .post("/graphql")
      .set("Authorization", `Bearer ${token}`)
      .send({
        query: `mutation BookEvent($eventId: ID!) {
          bookEvent(eventId:$eventId){
            _id
            event {
            _id
            }
            user {
            _id
            }
            createdAt
            updatedAt
          } 
        }`,
        variables: {
          eventId: "5d959694239bd335ec50e7f0"
        }
      })
      .expect(500)
      .end((err, res) => {
        if (err) return done(err);
        if (res.body.errors) {
          expect(res.body.errors[0].message).eql("Event not exists.");
        }
        done();
      });
  });
  it("Book an event (existing event)", done => {
    request
      .post("/graphql")
      .set("Authorization", `Bearer ${token}`)
      .send({
        query: `mutation BookEvent($eventId: ID!) {
          bookEvent(eventId:$eventId){
            _id
            event {
            _id
            }
            user {
            _id
            }
            createdAt
            updatedAt
          } 
        }`,
        variables: {
          eventId: eventId
        }
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        bookingId = res.body.data.bookEvent._id;
        done();
      });
  });
  it("Cancel booking", done => {
    request
      .post("/graphql")
      .set("Authorization", `Bearer ${token}`)
      .send({
        query: `mutation CancelBooking ($bookingId: ID!) {
          cancelBooking(bookingId:$bookingId) {
            _id
          }
        }`,
        variables: {
          bookingId: bookingId
        }
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        if (res.body.errors) {
          expect(res.body.errors[0].message).eql("Event not exists.");
        }
        done();
      });
  });
});
