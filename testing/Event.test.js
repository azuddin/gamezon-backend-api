const { expect } = require("chai");
const faker = require("faker");

const { dateToString } = require("../helpers/date");

const url = `http://localhost:3001/graphql`;
const request = require("supertest")(url);

let token;

const email = "test@domain.com";
const title = "Test " + faker.lorem.sentence();
const description = faker.lorem.sentences();
const price = parseFloat(faker.finance.amount(10, 99, 2));
const date = dateToString(faker.date.recent());

before(async () => {
  const login = await request.post("/graphql").send({
    query: `query Login($email: String!, $password: String!) {
      login(email:$email,password:$password)
      {
        userId
        token
        tokenExpiration
      }
    }`,
    variables: {
      email,
      password: "zaq1xsw2"
    }
  });
  token = login.body.data.login.token;
});

describe("Test Event Module", () => {
  it("Create Event", done => {
    request
      .post("/graphql")
      .set("Authorization", `Bearer ${token}`)
      .send({
        query: `mutation CreateEvent($title: String!, $description: String!, $price: Float!, $date: String!) {
          createEvent(eventInput:{title:$title, description:$description, price:$price, date:$date}) 
          { 
            _id
          } 
        }`,
        variables: {
          title,
          description,
          price,
          date
        }
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });
  it("List all events", done => {
    request
      .post("/graphql")
      .send({
        query: `{ 
          events {
            data {
              _id
              title
              description
              price
              date
              creator {
                email
              }
            }
            metadata {
              total
              lastPageNum
            }
          }
        }`
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.data.events).to.be.an("object");
        expect(res.body.data.events.data).to.be.an("array");

        const sample = { ...res.body.data.events.data[0] };
        expect(sample).to.have.property("_id");
        expect(sample).to.have.property("title");
        expect(sample).to.have.property("description");
        expect(sample).to.have.property("price");
        expect(sample).to.have.property("date");
        expect(sample).to.have.property("creator");

        expect(res.body.data.events.metadata).to.have.property("total");
        expect(res.body.data.events.metadata).to.have.property("lastPageNum");

        done();
      });
  });
});
