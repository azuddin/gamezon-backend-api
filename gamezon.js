const express = require("express");
const Sentry = require("@sentry/node");
const bodyParser = require("body-parser");
const graphqlHttp = require("express-graphql");
const mongoose = require("mongoose");

require("dotenv").config();

const graphQLSchema = require("./graphql/schema/index");
const graphQLResolver = require("./graphql/resolver/index");
const isAuth = require("./middleware/is-auth");

const app = express();

Sentry.init({
  dsn: "https://73b7012ee9314d799c48e238c39410b2@sentry.io/1840011"
});
app.use(Sentry.Handlers.requestHandler());

if (process.env.NODE_ENV === "development") {
  var cors = require("cors");
  app.use(cors());
}

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  if (req.method === "OPTIONS") {
    return res.sendStatus(200);
  }
  next();
});

app.use(isAuth);

// const extensions = ({
//   document,
//   variables,
//   operationName,
//   result,
//   context
// }) => {
//   console.log(document, variables, operationName, result, context);
//   return {
//     runTime: Date.now() - context.startTime
//   };
// };

/**
 * the endpoint name can be change to anything. for now its /graphql
 */
app.use(
  "/graphql", // endpoint name
  graphqlHttp({
    schema: graphQLSchema,
    rootValue: graphQLResolver,
    graphiql: true // to show in-browser IDE for exploring GraphQL
    // extensions,
    // context: { startTime: Date.now() }
  })
);

app.use(express.static(__dirname + "/public"));

app.use(Sentry.Handlers.errorHandler());

mongoose.set("useFindAndModify", false); // to fix deprecation warning of findOndAndUpdate()
mongoose
  .connect(
    `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster-zzaua.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`,
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(() => {
    console.log("> connected to mongodb.");
    const PORT = process.env.PORT || 3001;
    app.listen(PORT);
    console.log(`> app is running on port ${PORT}`);
  })
  .catch(err => {
    console.log(err);
  });
