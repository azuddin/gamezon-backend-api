const { buildSchema } = require("graphql");

// put top for new model
module.exports = buildSchema(`

type DeviceData {
  deviceID: ID!
  serviceUUID: String!
  characteristicUUID: String!
}

"Device model"
type Device {
  _id: ID!
  serviceUUID: String!
  characteristicUUID: String!
  deviceID: String!
  owner: User
  createdAt: String!
  updatedAt: String!
}

"Collection of devices"
type Devices {
  data: [Device!]!
  metadata: PaginateMeta!
}

"Booking model"
type Booking {
  _id: ID!
  event: Event!
  user: User!
  createdAt: String!
  updatedAt: String!
}

"Event model"
type Event {
  _id: ID!
  title: String!
  description: String!
  price: Float!
  date: String!
  creator: User!
  createdAt: String!
  updatedAt: String!
}

"Collection of events"
type Events {
  data: [Event!]!
  metadata: PaginateMeta!
}

"User model"
type User {
  _id: ID!
  email: String!
  password: String
  createdEvents: [Event!]
  createdAt: String!
  updatedAt: String!
}

"Metadata for pagination"
type PaginateMeta {
  total: Int!
  pageSize: Int!
  currPageNum: Int!
  lastPageNum: Int!
}

"Filtering input"
input Filter {
  columnName: String!
  value: String!
}

"Sorting input"
input Sort {
  columnName: String!
  value: Int!
}

"Authentication data schema"
type AuthData {
  userId: ID!
  token: String!
  tokenExpiration: Int!
}

input EventInput {
  title: String!
  description: String!
  price: Float!
  date: String!
}

input UserInput {
  email: String!
  password: String!
}

"Query methods"
type RootQuery {
  "Generate UUID for a device"
  generateDevicesUUID(deviceName:String!): DeviceData!
  "List of devices"
  devices(pageSize:Int, pageNum:Int, filter: [Filter], sort: [Sort]): Devices!
  "List of events"
  events(pageSize:Int, pageNum:Int, filter: [Filter], sort: [Sort]): Events!
  "List of bookings"
  bookings: [Booking!]!
  "List of users"
  users: [User!]!
  "Login user"
  login(email: String!, password: String!): AuthData!
}

"Mutation methods"
type RootMutation {
  "Register a device"
  registerDevice(deviceID: String!): Device!
  "Create an events"
  createEvent(eventInput:  EventInput): Event
  "Create an user"
  createUser(userInput:  UserInput): User
  "Booking an event for an user"
  bookEvent(eventId:  ID!): Booking!
  "Cancel a booking"
  cancelBooking(bookingId:  ID!): Event!
}

schema {
  query:  RootQuery
  mutation:  RootMutation
}
`);
