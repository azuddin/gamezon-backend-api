const Device = require("../../models/device");
const Devices = require("../../models/devices");
const User = require("../../models/user");
const { paginate, transformDevice } = require("./merge");

module.exports = {
  generateDevicesUUID: async ({ deviceName }, req) => {
    if (!req.isAuth) {
      throw new Error("Unauthenticated");
    }
    const uuidv1 = require("uuid/v1");
    const uuidv5 = require("uuid/v5");
    const serviceUUID = uuidv5(`service-${deviceName}`, uuidv1());
    const characteristicUUID = uuidv5(`characteristic-${deviceName}`, uuidv1());
    const DATETIME = Date.now();

    const device = new Device({
      serviceUUID,
      characteristicUUID,
      deviceID: `GMZON${DATETIME}`
    });
    try {
      const result = await device.save();

      return transformDevice(result);
    } catch (err) {
      throw err;
    }
  },
  devices: async (args, req) => {
    if (!req.isAuth) {
      throw new Error("Unauthenticated");
    }
    try {
      const { data, meta } = await paginate(Device, args);
      const result = new Devices({
        data: data.map(item => {
          return transformDevice(item);
        }),
        metadata: meta
      });

      return result;
    } catch (err) {
      throw err;
    }
  },
  registerDevice: async ({ deviceID }, req) => {
    if (!req.isAuth) {
      throw new Error("Unauthenticated");
    }
    try {
      const result = await Device.findOneAndUpdate(
        { deviceID, owner: null },
        { owner: req.userId }
      );
      if (!result) {
        throw new Error(
          "Device is invalid or already attached to another user."
        );
      }
      return transformDevice({ ...result, id: result.id, owner: req.userId });
    } catch (err) {
      throw err;
    }
  }
};
