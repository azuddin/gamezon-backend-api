const authResolver = require("./auth");
const bookingResolver = require("./booking");
const eventResolver = require("./event");
const deviceResolver = require("./device");

const rootResolver = {
  ...authResolver,
  ...bookingResolver,
  ...eventResolver,
  ...deviceResolver
};

module.exports = rootResolver;
