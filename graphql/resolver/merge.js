const DataLoader = require("dataloader");

const Event = require("../../models/event");
const User = require("../../models/user");

const PaginateMeta = require("../../models/paginateMeta");

const { dateToString } = require("../../helpers/date");

const eventLoader = new DataLoader(eventIds => {
  return events(eventIds);
});

const userLoader = new DataLoader(userIds => {
  return User.find({ _id: { $in: userIds } });
});

const events = async eventIds => {
  try {
    const result = await Event.find({ _id: { $in: eventIds } });
    return result.map(event => {
      return transformEvent(event);
    });
  } catch (err) {
    throw err;
  }
};

const singleEvent = async eventId => {
  try {
    const event = await eventLoader.load(eventId.toString());
    return event;
  } catch (err) {
    throw err;
  }
};

const user = async userId => {
  try {
    const result = await userLoader.load(userId.toString());
    return {
      ...result._doc,
      _id: result.id,
      createdEvents: () => eventLoader.loadMany(result._doc.createdEvents)
    };
  } catch (err) {
    throw err;
  }
};

const transformDevice = device => {
  return {
    ...device._doc,
    _id: device.id,
    owner: device.owner ? user.bind(this, device.owner) : null
  };
};

const transformEvent = event => {
  return {
    ...event._doc,
    _id: event.id,
    date: dateToString(event._doc.date),
    creator: event.creator ? user.bind(this, event.creator) : null
  };
};

const transformBooking = booking => {
  return {
    ...booking._doc,
    _id: booking.id,
    user: user.bind(this, booking._doc.user._id),
    event: singleEvent.bind(this, booking._doc.event._id),
    createdAt: dateToString(booking._doc.createdAt),
    updatedAt: dateToString(booking._doc.updatedAt)
  };
};

const transformFilter = array =>
  array.reduce((obj, item) => {
    obj[item.columnName] = { $regex: new RegExp(item.value), $options: "<i>" };
    return obj;
  }, {});

const transformSort = array =>
  array.reduce((obj, item) => {
    if (item.value !== 1 && item.value !== -1) {
      throw new Error(
        "Sorting value must be 1 for ascending OR -1 for descending."
      );
    }
    obj[item.columnName] = item.value;
    return obj;
  }, {});

/**
 * Pagination request sample:
 * {
 *   sample(pageSize: 1, pageNum: 1, filter: [{columnName: "title", value: "Testing"}], sort: [{columnName: "_id", value: 1}]) {
 *     data {
 *       _id
 *       title
 *       description
 *       price
 *       creator {
 *         email
 *       }
 *     }
 *     metadata {
 *       total
 *       lastPageNum
 *     }
 *   }
 * }
 * Optional arguments:
 * - pageSize
 * - pageNum
 * - filter
 * - sort
 */
const paginate = async (Model, args) => {
  try {
    const _pageSize = args.pageSize || 10;
    const _pageNum = args.pageNum || 1;
    const _filter = args.filter ? transformFilter(args.filter) : {};
    const _sort = args.sort ? transformSort(args.sort) : {};
    const count = await Model.countDocuments(_filter);
    const data = await Model.find(_filter)
      .skip(_pageSize * (_pageNum - 1))
      .limit(_pageSize)
      .sort(_sort);

    const meta = new PaginateMeta();
    meta.total = count;
    meta.pageSize = _pageSize;
    meta.currPageNum = _pageNum;
    meta.lastPageNum = Math.ceil(count / _pageSize);

    return { data, meta };
  } catch (err) {
    throw err;
  }
};

exports.paginate = paginate;
exports.transformDevice = transformDevice;
exports.transformBooking = transformBooking;
exports.transformEvent = transformEvent;
exports.transformFilter = transformFilter;
exports.transformSort = transformSort;
exports.eventLoader = eventLoader;
