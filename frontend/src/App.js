import React, { Component } from "react";
import "./App.sass";
let DEV_URL = "";
if (process.env.NODE_ENV === "development") {
  DEV_URL = "http://localhost:3001";
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      menuIsActive: false
    };
  }
  // async componentDidMount() {
  //   // Call self-hosted API to get response
  //   const res = await fetch(`${DEV_URL}/graphql`, {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json"
  //     },
  //     body: JSON.stringify({
  //       query: `query {
  //         events {
  //           data {
  //             _id
  //             title
  //             description
  //             price
  //             date
  //             creator {
  //               email
  //             }
  //           }
  //           metadata {
  //             total
  //             lastPageNum
  //           }
  //         }
  //       }`
  //     })
  //   });
  //   const result = await res.json();
  //   const data = result.data.events.data;
  //   console.log(result.data.events.data);
  //   this.setState({
  //     data
  //   });
  // }
  render() {
    const { menuIsActive } = this.state;
    return (
      <div className="App">
        <nav className="navbar" role="navigation" aria-label="main navigation">
          <div className="navbar-brand">
            <a className="navbar-item" href="https://bulma.io">
              <img src={require("./assets/logo.png")} width="112" height="28" />
            </a>
            <a className="navbar-item" style={{ marginLeft: "auto" }}>
              <button className="button is-primary is-outlined is-hidden-desktop">
                Try it free
              </button>
            </a>
            <a
              role="button"
              className={
                "navbar-burger burger is-marginless" +
                (menuIsActive ? " is-active" : "")
              }
              aria-label="menu"
              aria-expanded="false"
              href="/"
              onClick={e => {
                e.preventDefault();
                this.setState({ menuIsActive: !menuIsActive });
              }}
            >
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
          </div>
          <div className={"navbar-menu" + (menuIsActive ? " is-active" : "")}>
            <div className="navbar-start">
              <a className="navbar-item">About</a>
              <a className="navbar-item">FAQ</a>
              <a className="navbar-item">Contact Us</a>
            </div>
            <div className="navbar-end">
              <div className="navbar-item">
                <div className="buttons">
                  <button className="button is-primary is-outlined has-text-weight-semibold">
                    Try it free
                  </button>
                  <button className="button is-light has-text-weight-semibold">
                    Log in
                  </button>
                </div>
              </div>
            </div>
          </div>
        </nav>
        <section class="hero is-medium is-bold ">
          <div class="hero-body">
            <div class="container">
              <div className="columns">
                <div className="column is-two-thirds">
                  <h1 class="title">
                    Book Sport Facilities.
                    <br />
                    Find Friendly Matches.
                    <br />
                    Effortlessly.
                  </h1>
                  <h2 class="subtitle">
                    GamezOn help you book any sport court &amp; find matches
                    nearby seamlessly.
                    <br />
                    Try it now for free.
                  </h2>
                </div>
                <div className="column">
                  <img
                    className="is-hidden-mobile"
                    style={{
                      position: "absolute",
                      width: "40%",
                      transform: "rotate(30deg)",
                      top: "-5rem"
                    }}
                    src={require("./assets/img1.png")}
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
export default App;
