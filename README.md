# Gamezon GraphQL API

A backend API using Graphql for Gamezon Project

- [Installation](#installation)
- [Usage](#usage)
- [Setup](#setup)
  - [PM2 setup](#pm2)
  - [Nginx conf setup](#nginx)
- [Tips](#tips)
  - [Just in case I forgot how to code](#forgot-to-code)

<a name="installation"></a>

## Installation

Install all dependencies

```sh
npm install
```

Install all dependencies for frontend

```sh
cd frontend && npm install
```

<a name="usage"></a>

## Usage

Run the server

```sh
# normal start
npm start

# start with nodemon
npm run dev
```

<a name="setup"></a>

## Setup

To setup node js application inside your server (in my case CentOS), you need to make sure node has been installed.

```bash
# check node version
node -v

#check npm version
npm -v
```

<a name="pm2"></a>

### PM2 Setup

Next, install PM2 (which is a process manager for Node.js applications). PM2 provides an easy way to manage and daemonize applications (run them as a service).

```bash
sudo npm i -g pm2@latest

# to start pm2 automatically when reboot
sudo pm2 startup systemd
```

To test & making sure its working & running, create this js file inside your /public_html folder:

```bash
cd /home/admin/web/gamezon.app/public_html
vi gamezon.js
```

```javascript
#!/usr/bin/env node
var http = require("http");
http
  .createServer(function(req, res) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("gamezon.app\n");
  })
  .listen(3000, "localhost");
console.log("Server running at http://localhost:3000/");
```

Then run

```bash
pm2 start gamezon.js

# to restart an app
pm2 restart gamezon

# to stop an app
pm2 stop gamezon

# to delete an app
pm2 delete gamezon

# to show pm2 process monitor
pm2 monit
```

This will adds your application to PM2’s process list, which is outputted every time you start an application:

```bash
Output
┌──────────┬────┬──────┬───────┬────────┬─────────┬────────┬─────────────┬──────────┐
│ App name │ id │ mode │ pid   │ status │ restart │ uptime │ memory      │ watching │
├──────────┼────┼──────┼───────┼────────┼─────────┼────────┼─────────────┼──────────┤
│ gamezon  │ 0  │ fork │ 30099 │ online │ 0       │ 0s     │ 14.227 MB   │ disabled │
└──────────┴────┴──────┴───────┴────────┴─────────┴────────┴─────────────┴──────────┘
```

Then restart your nginx

```bash
service nginx restart
```

<a name="nginx"></a>

### Nginx config setup

Change the `proxy_pass` port to the port you want to use (in my case its 3000) inside nginx.conf / nginx.ssl.conf

```bash
vi /home/admin/conf/web/gamezon.app.nginx.ssl.conf
```

```nginx
...

    location / {
        proxy_pass      http://localhost:3001;#(i)
        location ~* ^.+\.(jpeg|jpg|png|gif|bmp|ico|svg|tif|tiff|css|js|htm|html|ttf|otf|webp|woff|txt|csv|rtf|doc|docx|xls|xlsx|ppt|pptx|odf|odp|ods|odt|pdf|psd|ai|eot|eps|ps|zip|tar|tgz|gz|rar|bz2|7z|aac|m4a|mp3|mp4|ogg|wav|wma|3gp|avi|flv|m4v|mkv|mov|mpeg|mpg|wmv|exe|iso|dmg|swf)$ {
            root           /home/admin/web/gamezon.app/public_html/public;#(ii)

...
```

1. Change the port number match with the port you're serving on.
2. In my case, i need to change the location of getting the assets to /public folder. If not, it will return 404 error.

<a name="githooks"></a>

### Git hooks setup

Here're steps on how to setup git hook post receive:

1.  ```bash
    cd /home/admin/web/gamezon.app/
    ```
2.  ```
    cd private/ && mkdir repo.git && cd repo.git/ && git init --bare && cd ../../
    ```
3.  ```
    vi private/repo.git/hooks/post-receive
    ```
4.  Paste and save these content:

    ```bash
    #!/bin/sh
    su admin
    git --work-tree=/home/admin/web/gamezon.app/public_html --git-dir=/home/admin/web/gamezon.app/private/repo.git checkout -f
    git --work-tree=/home/admin/web/gamezon.app/public_html --git-dir=/home/admin/web/gamezon.app/private/repo.git pull
    cd /home/admin/web/gamezon.app/public_html
    npm install --only=prod
    echo "Hooray, the new version is published!"
    ```

5.  ```
    chmod +x private/repo.git/hooks/post-receive
    ```

6.  When you first configure this, you’ll need to make a first git clone manually to your **public_html/** directory (if this directory already exists, either move it temporarily (as a back-up) or remove it altogether).

    ```bash
    git clone private/repo.git public_html
    ```

7.  From within your local repo:

    ```bash
    git remote add live ssh://root@gamezon.app/home/admin/web/gamezon.app/private/repo.git
    ```

8.  Now, whenever you do the following, ...
    ```bash
    git commit -m "your commit message"
    git push live master
    ```
    ... your server will receive the data from the git push and will trigger its post-receive you have configured above.

<a name="tips"></a>

## Tips:

<a name="forgot-to-code"></a>

### Just in case I forgot how to code 😵

- Here're steps to create a new feature:
  1. Create a model
  2. Define it in schema
  3. Construct the resolver
