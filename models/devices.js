const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const scheme = new Schema(
  {
    data: {
      type: Array,
      required: true
    },
    metadata: {
      type: Object,
      required: true
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Devices", scheme);
