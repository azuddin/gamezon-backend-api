const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const schema = new Schema(
  {
    serviceUUID: {
      type: String,
      required: true
    },
    characteristicUUID: {
      type: String,
      required: true
    },
    deviceID: {
      type: String,
      required: true
    },
    owner: {
      type: Schema.Types.ObjectId,
      ref: "User"
      // default: null
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Device", schema);
