const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const paginateScheme = new Schema(
  {
    total: {
      type: Number,
      required: true
    },
    pageSize: {
      type: Number,
      required: true
    },
    currPageNum: {
      type: Number,
      required: true
    },
    lastPageNum: {
      type: Number,
      required: true
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("PaginateMeta", paginateScheme);
